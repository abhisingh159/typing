<div class="row">
	<div class='col-md-6'>
    {!! $records->render() !!}
    </div>
	<div class='col-md-6' style='padding-top:20px; text-align: right'>
    	<b>Showing {{$records->firstItem()}} - {{$records->lastItem()}} of {{$records->total()}}</b>
    </div>
</div>
