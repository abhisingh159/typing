<div class="container-fluid bg-dark">
    <div class="container">
      <div class="row pb-3">
        <div class="col-sm-12  pt-5">
          <div class="row d-flex">
            <div class="col-sm-4">
              <h3 class="logo-text footer-logo">
                <a href="#;">Typing</a>
              </h3>
            </div>
            <div class="col-md-4">
              <ul class="custom-list">
                <li><a href="{{route('about-Us')}}">About us</a></li>
                <li><a href="{{route('contact-Us')}}">Contact Us</a></li>
                <li><a href="{{route('faq')}}">FAQ</a></li>
                <li><a href="{{route('terms')}}">Terms & Conditions</a></li>
              </ul>
            </div>
            <div class="col-sm-4">
              <div class="row">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-12">
                      <h5 class="text-white">Share</h5>
                    </div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-12 footer-d-i">
                          <a href="#"><i class="fab fa-facebook"></i></a>
                          <a href="#"><i class="fab fa-twitter-square"></i></a>
                          <a href="#"><i class="fab fa-linkedin"></i></a>
                          <a href="#"><i class="fab fa-youtube-square"></i></a>
                          <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid end-bootam">
    <div class="container">
      <div class="row p-3 text-white">
        <div class="col-sm-12 text-center">Copyright &copy; {{date('Y')}} <small> Powered By <a href="https://www.achieversaim.com" class="text-danger">Achiversaim</a> </small>   </div>
      </div>
    </div>
  </div>