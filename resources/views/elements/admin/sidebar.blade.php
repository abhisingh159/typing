<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{env("APP_URL")}}" class="brand-link">
    <img src="{{asset("assets/dist/img/AdminLTELogo.png")}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset("assets/dist/img/user2-160x160.jpg")}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{route('dashboard.index')}}" class="nav-link {{ (request()->is('admin/dashboard')) ? ' active' : '' }}">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview {{ (request()->is('admin/contest*')) ? ' menu-open' : '' }}">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Contest
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{route('contest.index')}}" class="nav-link {{ (request()->is('admin/contest')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>List</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('contest.running')}}" class="nav-link {{ (request()->is('admin/contest/running')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Running</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('contest.create')}}" class="nav-link {{ (request()->is('admin/contest/create')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Create</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview {{ (request()->is('admin/user*')) ? ' menu-open' : '' }}">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Users
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{route('user.index')}}" class="nav-link {{ (request()->is('admin/user')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>List</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('users.block')}}" class="nav-link {{ (request()->is('admin/users/block')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Blocked Users</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview {{ (request()->is('admin/redeem*')) ? ' menu-open' : '' }}">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Redeem Request
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{route('admin.redeem')}}" class="nav-link {{ (request()->is('admin/redeem')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>List</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview {{ (request()->is('admin/contact*')) ? ' menu-open' : '' }}">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Contact Us
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{route('contact.index')}}" class="nav-link {{ (request()->is('admin/contact')) ? ' active' : '' }}">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>List</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="{{route('admin.logout')}}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Logout
            </p>
          </a>
          <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                style="display: none;">{{ csrf_field() }}</form>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
