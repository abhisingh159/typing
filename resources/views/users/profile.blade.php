@extends('layouts.users')

@section('page-title', $title)

@section('content')
    <a href="{{ route('user.edit', Auth::id()) }}" class="btn btn-primary mb-3">Update Profile</a>
    <div class="row">

        <div class="col-sm-6">

            <h5 class="section-title">User Details</h5>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Name :</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$user->name}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Email :</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$user->email}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Mobile :</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$user->mobile}}">
                </div>
            </div>

        </div>

        <div class="col-sm-6">

            <h5 class="section-title">Accounts Details</h5>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">A/C No. :</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$user->account_number}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">IFSC :</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$user->ifsc}}">
                </div>
            </div>

        </div>

    </div>
@stop