@extends('layouts.users')

@section('page-title', $title)

@section('content')
    <div class="earning-page py-5">
        <div class="row">

            <div class="col-sm-6">

                <h5 class="section-title">Balance & Redeems</h5>
                <div class="section-content p-3">
                    <div class="balance mt-3">
                        <div class="row">
                            <div class="col-6">
                                <h6 class="mt-2">Total Balance : </h6>
                            </div>
                            <div class="col-6">
                                <div class="d-flex justify-content-between align-items-center">
                                    <strong> &#8377; {!! $user->balance !!}</strong>
                                    <button class="btn btn-success" data-target="#redeem-form" data-toggle="modal">Redeem</button>
                                </div>
                            </div>
                        </div>

                        <div class="redeems-container mt-4">
                            <ul class="nav nav-tabs mb-3" id="redeemTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab"
                                        aria-controls="all"
                                        aria-selected="true">All Redeems</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="requested-tab" data-toggle="tab" href="#requested" role="tab"
                                        aria-controls="requested" aria-selected="false">Requested Redeems</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab"
                                        aria-controls="completed" aria-selected="false">Completed Redeems</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="redeemTab">

                                <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                    <table class="table">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Requested At</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Redeemed At</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($redeems as $key => $redeem)
                                            <tr>
                                                <th scope="row">{!! $key+1 !!}</th>
                                                <td>&#8377; {!! $redeem->amount !!}</td>
                                                <td><small>{!! $redeem->created_at !!}</small></td>
                                                <td>{!! $redeem->status !!}</td>
                                                <td><small>{!! $redeem->updated_at !!}</small></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="requested" role="tabpanel" aria-labelledby="requested-tab">
                                    <table class="table">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Requested At</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Redeemed At</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($redeem_request as $key => $redeem)
                                            <tr>
                                                <th scope="row">{!! $key+1 !!}</th>
                                                <td>&#8377; {!! $redeem->amount !!}</td>
                                                <td><small>{!! $redeem->created_at !!}</small></td>
                                                <td>{!! $redeem->status !!}</td>
                                                <td><small>{!! $redeem->updated_at !!}</small></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="completed" role="tabpanel" aria-labelledby="completed-tab">
                                    <table class="table">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Requested At</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Redeemed At</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($redeemed as $key => $redeem)
                                            <tr>
                                                <th scope="row">{!! $key+1 !!}</th>
                                                <td>&#8377; {!! $redeem->amount !!}</td>
                                                <td><small>{!! $redeem->created_at !!}</small></td>
                                                <td>{!! $redeem->status !!}</td>
                                                <td><small>{!! $redeem->updated_at !!}</small></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-6">

                <h5 class="section-title">Winnings</h5>
                <div class="sections-content p-3">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Contest</th>
                            <th scope="col">Rank</th>
                            <th scope="col">Points</th>
                            <th scope="col">Winnings</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($user->typingTests as $key => $typingTest)
                            <tr>
                                <th scope="row">{!! $key+1 !!}</th>
                                <td>{!! $typingTest->contest->title !!}</td>
                                <td>{!! ($typingTest->rank == 0) ? '-' : $typingTest->rank !!}</td>
                                <td>{!! $typingTest->points !!}</td>
                                <td>{!! ($typingTest->amount == 0) ? '-' :  '&#8377; '.$typingTest->amount !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
    @include('modals.redeem-form')
@endsection