<div class="modal fade" id="redeem-form">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="text-center">Redeem Request</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            @if($user->balance > 100 )
                @if(($user->account_number != null) || ($user->ifsc != null))
                    {!! Form::open(['route' => 'redeem.store', 'method' => 'post']) !!}

                        <!-- Modal body -->
                        <div class="modal-body">

                                {!! Form::hidden('user_id', Auth::id(), ['class' => 'form-control']) !!}
                                {!! Form::hidden('status', 0, ['class' => 'form-control']) !!}

                            <div class="form-group">
                                {!! Form::label('amount', 'Amount ', ['class' => 'control-label']) !!}
                                <small class="" title="Amount should be between this."> (&#8377; 100 - &#8377; {!! $user->balance !!} )</small>
                                {!! Form::text('amount', '', ['class' => 'form-control', 'placeholder' => 'Enter amount to redeem']) !!}
                                <small id="amountHelpBlock" class="form-text text-muted">
                                    Please enter minimum &#8377; 100 and not more then your wallet balance!!
                                </small>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            {!! Form::submit('Redeem Request', ['class' => 'form-control btn btn-success']) !!}
                        </div>

                    {!! Form::close() !!}

                @else
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <p>You didn't updated your bank account number and IFSC in your profile.</p>
                                <p>Please update bank account details for successful redemption. </p>
                                <p>For update back account details goto your profile and  update with correct details.</p>
                                <p>To update now <a href="{!! route('user.edit', Auth::id()) !!}" class="btn btn-outline-success btn-sm">click here</a></p>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                @endif
            @else

                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        You don't have sufficient balance for redeem request. <br>
                        You should have minimum &#8377; 100 balance for redeem request.
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            @endif

        </div>
    </div>
</div>