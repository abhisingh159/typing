<div class="row contest-details">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xm-12 col-md-12 mb-3">
                <div class="single-contest">
                    <h4>{!! $contest->title !!}</h4>
                    <p>{!! $contest->description !!}</p>
                </div>
                <div class="col-md-12 home-match-card4">
                    <div class="row justify-content-center align-items-center pt-3">
                        <div class="col-sm-6 col-6 text-left ">
                            <small class="text-muted mb-1">Total Prize</small>
                            <h5 class="m-0"><i class="fas fa-rupee-sign"></i> {!! $contest->total_prize !!}</h5>
                        </div>
                        <div class="col-sm-6 col-6 text-right">
                            <small class="text-muted mb-1">Entry</small>
                            <form action="{{route('order',$contest->id)}}" method="post">
                                <input type="hidden" name="fees" value="{{$contest->entry_fees}}">
                                @if ((Auth::check() &&  $contest->hasUser(Auth::user()->id)) || ($contest->reg_users == $contest->max_users ))
                                    <button type="submit" class="btn btn-success join" disabled><i class="fas fa-rupee-sign"></i> {{$contest->entry_fees}}</button>
                                @else
                                    <button type="submit" class="btn btn-success join"><i class="fas fa-rupee-sign"></i> {{$contest->entry_fees}}</button>
                                @endif
                            </form>
                        </div>
                    </div>
                    <div class="progress mt-3" style="height:8px">
                        <div class="progress-bar bg-gold" style="width:{{ ($contest->reg_users / $contest->max_users) * 100  }}%"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-6 text-left text-gold"><p>{{($contest->max_users - $contest->reg_users)}} soprt left</p></div>
                        <div class="col-sm-6 col-6 text-right text-muted home-prize"><p>{{$contest->max_users}} spots</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col text-center mt-3 mb-1">
                <ul class="nav nav-pills mb-3 nav-fill" id="leaderboardTab" role="tablist">
                    <li class="nav-item">
                        <h5 class="nav-link active" id="prize-tab" data-toggle="tab" data-target="#prize" role="tab"
                            aria-controls="prize"
                            aria-selected="true">Prize Breakup</h5>
                    </li>
                    <li class="nav-item">
                        <h5 class="nav-link" id="ranking-tab" data-toggle="tab" data-target="#ranking" role="tab"
                            aria-controls="ranking" aria-selected="false">Leaderboard</h5>
                    </li>
                </ul>
                {{--<h5>Prize Breakup</h5>--}}
                <div class="tab-content" id="leaderboard">

                    <div class="tab-pane fade show active" id="prize" role="tabpanel" aria-labelledby="prize-tab">
                        <table class="table">
                            <tr>
                                <th>Rank</th>
                                <th class="text-right">Prize</th>
                            </tr>
                            @foreach ($contest->prizings as $prizing)
                                <tr>
                                    <td>
                                        # {!! $prizing->rank_from  !!} {!! (($prizing->rank_to != 0) ? ' - '.$prizing->rank_to : '') !!}</td>
                                    <td class="text-right"><i class="fas fa-rupee-sign"></i> {!! $prizing->amount !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="tab-pane fade" id="ranking" role="tabpanel" aria-labelledby="ranking-tab">
                        <table class="table">
                            <tr>
                                <th>Users</th>
                                <th>Points</th>
                                <th>Rank</th>
                            </tr>
                            @foreach ($rankings as $ranking)
                                <tr>
                                    <td>{!! $ranking->user->name !!}</td>
                                    <td>{!! $ranking->points !!}</td>
                                    <td>{!! $ranking->rank !!}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>