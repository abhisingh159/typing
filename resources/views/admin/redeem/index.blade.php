@extends('layouts.admin')

@section('title', 'Users List')

@section('pageHeader', 'Users Contest')

@section('content')
    <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">All Users</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Amount</th>
              <th>Paid</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
            @foreach ($redeems as $key => $redeem)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$redeem->amount}}</td>
                    <td>{{$redeem->user->email}}</td>
                    <td>{{$redeem->user->mobile}}</td>
                    <td>{{ $redeem->amount }}</td>
                    <td><?= ($redeem->status == 1) ? "<span class='badge badge-success'>Paid</span>" : "<span class='badge badge-danger'>Pending</span>" ?></td>
                    <td>{{$redeem->created_at}}</td>
                    <td>
                        @if($redeem->status == 0)
                            <a href="{{route('admin.redeem.pay',$redeem->id)}}" title="Pay"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $redeems->links() }}
        </div>
      </div>
    </div>
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
@endsection
