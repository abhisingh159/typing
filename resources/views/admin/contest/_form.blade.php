<div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{!! __('Contest Description') !!}</h3>
            </div>
            
            <div class="card-body">

                <div class="form-group">
                    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control '.(($errors->has("title")) ? 'is-invalid' : ''),'placeholder' => 'Enter Title', 'disabled' => $disable]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control '.(($errors->has("description")) ? 'is-invalid' : '' ),'placeholder' => 'Enter Description', 'rows' => 3, 'cols' => 54]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('typing_text', 'Typing Text', ['class' => 'control-label']) !!}
                    {!! Form::textarea('typing_text', null, ['class' => 'form-control '.(($errors->has("typing_text")) ? 'is-invalid' : ''),'placeholder' => 'Enter Typing Text', 'rows' => 3, 'cols' => 54]) !!}
                </div>

                <div class="row"> {{-- .row --}}
                    <div class="col-md-6"> {{-- .col-md-6  --}}
                        <div class="form-group">
                            {!! Form::label('start_time', 'Start Time', ['class' => 'control-label']) !!}
                            <div class="input-group date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                {!! Form::text('start_time', (isset($contest->start_time)) ? $contest->start_time->format('Y-m-d H:m:s') : old('start_time'), ['class' => 'form-control datepicker '.(($errors->has('start_date')) ? 'is-invalid' : ''), 'placeholder' => 'YYYY-MM-DD', 'disabled' => $disable ]) !!}
                            </div>
                        </div>
                    </div> {{-- .col-md-6  --}}
                    <div class="col-md-6"> {{-- .col-md-6  --}}
                        <div class="form-group">
                            {!! Form::label('end_time', 'End Time', ['class' => 'control-label']) !!}
                            <div class="input-group date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                {!! Form::text('end_time', (isset($contest->end_time)) ? $contest->end_time->format('Y-m-d H:m:s') : old('end_time'), ['class' => 'form-control datepicker  '.(($errors->has("end_date")) ? 'is-invalid' : ''), 'placeholder' => 'YYYY-MM-DD', 'disabled' => $disable ]) !!}
                            </div>
                        </div>
                    </div> {{-- .col-md-6  --}}
                </div> {{-- .row --}}

                <div class="form-group">
                    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'), '0',['class' => 'form-control']); !!}
                </div>

            </div>
            
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-secondary">
            
            <div class="card-header">
                <h3 class="card-title">{!! __('Contest Prize') !!}</h3>
            </div>
            
            <div class="card-body"> {{-- .card-body --}}

                <div class="row"> {{-- .row  --}}
                    <div class="col-md-4"> {{-- .col-md-4  --}}
                        <div class="form-group">
                            {!! Form::label('total_prize', 'Prizing Amount', ['class' => 'control-label']) !!}
                            {!! Form::text('total_prize', null, ['class' => 'form-control '.(($errors->has("total_prize")) ? 'is-invalid' : ''), 'placeholder' => 'Enter Prizing Amount', 'disabled' => $disable]) !!}
                        </div>
                    </div> {{-- .col-md-4  --}}
                    <div class="col-md-4"> {{-- .col-md-4 --}}
                        <div class="form-group">
                            {!! Form::label('entry_fees', 'Entry Fees', ['class' => 'control-label']) !!}
                            {!! Form::text('entry_fees', null, ['class' => 'form-control '.(($errors->has("entry_fees")) ? 'is-invalid' : '' ), 'placeholder' => 'Enter Entry Fees', 'disabled' => $disable]) !!}
                        </div>
                    </div> {{-- .col-md-4 --}}
                    <div class="col-md-4"> {{-- .col-md-4 --}}
                        <div class="form-group">
                            {!! Form::label('max_users', 'Max Users', ['class' => 'control-label']) !!}
                            {!! Form::text('max_users', null, ['class' => 'form-control '.(($errors->has("max_users")) ? ' is-invalid' : '') , 'placeholder' => 'Max Number of Users', 'disabled' => $disable]) !!}
                        </div>
                    </div> {{-- .col-md-4 --}}
                </div> {{-- .row --}}

                <h5 class="mt-4 mb-3">{!! __('Prize Breakup') !!}</h5>

                @if (isset($contest))
                    @foreach ($contest->prizings as $prizing)
                        <div class="input-box">
                            <div class="row input-row"> {{-- .row  --}}
                                <div class="col-md-4 form-group"> {{-- .col-md-4  --}}
                                    {!! Form::label('from[]', 'Rank From', ['class' => 'control-label']) !!}
                                    {!! Form::text('from[]', $prizing->rank_from, ['class' => 'form-control '.(($errors->has('from[]')) ? ' is-invalid' : ''), 'placeholder' => 'Enter From Rank', 'disabled' => $disable]) !!}
                                </div> {{-- .col-md-4  --}}
                                <div class="col-md-4 form-group"> {{-- .col-md-4  --}}
                                    {!! Form::label('to[]', 'Rank To', ['class' => 'control-label']) !!}
                                    {!! Form::text('to[]', $prizing->rank_to, ['class' => 'form-control'.(($errors->has('to[]')) ? ' is-invalid' : ''), 'placeholder' => 'Enter To Rank', 'disabled' => $disable]) !!}
                                </div> {{-- .col-md-4 --}}
                                <div class="col-md-4 form-group"> {{-- .col-md-4  --}}
                                    {!! Form::label('prize[]', 'Prizing Amount', ['class' => 'control-label']) !!}
                                    {!! Form::text('prize[]', $prizing->amount, ['class' => 'form-control'.(($errors->has('prize[]')) ? ' is-invalid' : ''), 'placeholder' => 'Enter Prizing Amount', 'disabled' => $disable]) !!}
                                </div> {{-- .col-md-4 --}}
                            </div> {{-- .row --}}
                        </div>
                    @endforeach
                @else
                    <div class="input-box">
                        <div class="row input-row"> {{-- .row  --}}
                            <div class="col-md-4 form-group"> {{-- .col-md-4  --}}
                                {!! Form::label('from[]', 'Rank From', ['class' => 'control-label']) !!}
                                {!! Form::text('from[]', null, ['class' => 'form-control '.(($errors->has('from[]')) ? 'is-invalid' : ''), 'placeholder' => 'Enter From Rank']) !!}
                            </div> {{-- .col-md-4  --}}
                            <div class="col-md-4 form-group"> {{-- .col-md-4  --}}
                                {!! Form::label('to[]', 'Rank To', ['class' => 'control-label']) !!}
                                {!! Form::text('to[]', null, ['class' => 'form-control'.(($errors->has('to[]')) ? 'is-invalid' : ''), 'placeholder' => 'Enter To Rank']) !!}
                            </div> {{-- .col-md-4 --}}
                            <div class="col-md-4 form-group"> {{-- .col-md-4  --}}
                                {!! Form::label('prize[]', 'Prizing Amount', ['class' => 'control-label']) !!}
                                {!! Form::text('prize[]', null, ['class' => 'form-control'.(($errors->has('prize[]')) ? 'is-invalid' : ''), 'placeholder' => 'Enter Prizing Amount']) !!}
                            </div> {{-- .col-md-4 --}}
                        </div> {{-- .row --}}
                    </div>
                    <div class="col-md-12">
                        <i class="btn btn-success clone-input fa fa-plus pull-right"></i>
                    </div>
                @endif

            </div> {{-- .card-body --}}

        </div>
    </div>
</div>