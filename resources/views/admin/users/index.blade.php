@extends('layouts.admin')

@section('title', 'Users List')

@section('pageHeader', 'Users Contest')

@section('content')
    <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">All Users</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Active</th>
              <th>Block</th>
              <th>Signup At</th>
              <th>Actions</th>
            </tr>
            @foreach ($usersList as $key => $users)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$users->name}}</td>
                    <td>{{$users->email}}</td>
                    <td>{{$users->mobile}}</td>
                    <td><?= ($users->is_varified == 1) ? "Active" : "InActive" ?></td>
                    <td><?= ($users->is_block == 1) ? "Blocked" : "Active" ?></td>
                    <td>{{$users->created_at}}</td>
                    <td>
                      <a href="{{route('user.delete',$users->id)}}"><i class="fa fa-trash"></i></a>
                      <a href="{{route('users.UnBlock',$users->id)}}">
                        <i class="fa fa-lock"></i>
                      </a>
                    </td>
                </tr>
            @endforeach
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
          </ul>
          {{ $usersList->links() }}
        </div>
      </div>
    </div>
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
@endsection
