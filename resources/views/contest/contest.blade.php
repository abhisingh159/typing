<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>{{$contest->title}}</title>
    <link href='https://fonts.googleapis.com/css?family=Cutive+Mono|Roboto:400,900,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style media="screen">
        /* $yellow: #2222ff;
        $red: #99f;
        $white: #fff; */

        /* body {
            padding-top: 20px;
            font-family: Roboto, sans-serif;
            text-align: center;
            background: #fff;
            color: #222;
            padding-bottom: 40px;
            background: url(https://djave.co.uk/hosted/subtlepatterns/lightpaperfibers.png);
        }

        #focus {
            background: rgba(255, 0, 0, 0.7);
            padding: 20px;
            font-weight: bold;
            font-size: 30px;
            color: #fff;
        }

        .mono {
            font-family: "Cutive Mono", monospace;
        }

        .wrong {
            background: $red;
            color: #fff;
        }

        hr {
            margin: 1em 0;
            max-width: 800px;
            border: none;
            border-top: 1px solid rgba(255, 255, 255, 0.3);
            margin: 0 auto;
        }

        .results {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
        }

        .stats {
            overflow: hidden;
            margin-bottom: 1em;
            height: 100px;
            list-style: none;
            padding: 5px 0;
            font-size: 16px;
            font-weight: 900;
            max-width: 1000px;
            margin: 0 auto;

            li {
                width: 25%;
                float: left;
            }
        }

        .target {
            color: #fff;
            text-align: left;
            font-size: 32px;
            min-width: 300px;
            min-height: 40px;
            width: 600px;
            display: inline-block;
            position: relative;
            white-space: pre;
            background: #333;
            box-shadow: inset 0 0 10px 0 rgba(255, 255, 255, 0.2);
            padding: 22px 10px 10px;

            &:after {
                content: '';
                position: absolute;
                width: 20px;
                height: 2px;
                background: #f00;
                left: 10px;
                top: 53px;
                -webkit-animation: cursor_flash .5s infinite;
                -moz-animation: cursor_flash .5s infinite;
                -o-animation: cursor_flash .5s infinite;
                animation: cursor_flash .5s infinite;
            }

            &:before {
                font-family: Roboto;
                top: 0;
                left: 0;
                right: 0;
                background: #000;
                content: 'Type the text below';
                text-transform: uppercase;
                font-size: 10px;
                padding: 2px;
                text-align: left;
                position: absolute;
                color: #fff;
            }
        }


        @-webkit-keyframescursor_flash {
            0% {
                background: $white;
            }

            50% {
                background: $red;
            }

            100% {
                background: $white;
            }
        }

        @-moz-keyframescursor_flash {
            0% {
                background: $white;
            }

            50% {
                background: $red;
            }

            100% {
                background: $white;
            }
        }

        @-o-keyframescursor_flash {
            0% {
                background: $white;
            }

            50% {
                background: $red;
            }

            100% {
                background: $white;
            }
        }

        @keyframescursor_flash {
            0% {
                background: $white;
            }

            50% {
                background: $red;
            }

            100% {
                background: $white;
            }
        }

        .button {
            display: inline-block;
            padding: 8px 10px;
            background: #057f24;
            border-radius: 2px;
            color: #fff;
        }

        #textarea {
            position: absolute;
        }

        .spell {
            font-size: 40px;
            font-weight: 900;
            letter-spacing: 2px;

            span {
                color: $yellow;
            }
        }

        textarea {
            color: #fff;
            width: 60%;
            margin: 1em auto;
            display: none;
            position: relative;
            white-space: pre;
            background: #333;
            box-shadow: inset 0 0 10px 0 rgba(255, 255, 255, 0.2);
            border: 1px solid rgba(255, 255, 255, 0.4);
        }

        .your-attempt {
            background: #222;
            color: #fff;
            padding: 10px;
            min-height: 100px;
            border: 1px solid #555;
            max-width: 600px;
            margin: 2em auto;
            white-space: pre-wrap;
        }

        .results {
            font-family: Roboto;
        }

        .settings {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 3px 6px 6px;
            background: #444;
            font-family: Roboto, sans-serif;
            color: #999;
            text-align: left;
            text-shadow: 0 0 6px rgba(0, 0, 0, 0.6);
            border-top: 3px solid #222;
            font-size: 12px;

            a {
                color: #fff;
                transition: color 0.2s;
                text-decoration: none;

                &:hover {
                    color: #ccf;
                }
            }
        }

        .twitter a {
            color: rgba(255, 0, 0, 0.7);
            text-decoration: none;

            &:hover {
                text-decoration: underline;
            }
        } */
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript">

        $(document).ready(function(){
        /*
         * The animation at the start, made from my previous pen
         * https://codepen.io/EightArmsHQ/pen/HJsav
         */

        // The base speed per character
        time_setting = 30;
        // How much to 'sway' (random * this-many-milliseconds)
        random_setting = 100;
        // The text to use NB use \n not real life line breaks!
        input_text = "How fast can you type?";
        // Where to fill up
        target_setting = $("#output");
        // Launch that function!
        type(input_text, target_setting, 0, time_setting, random_setting);

        function type(input, target, current, time, random) {
            // If the current count is larger than the length of the string, then for goodness sake, stop
            if (current > input.length) {
                // Write Complete
                console.log("Complete.");
            } else {
                // console.log(current)
                // Increment the marker
                current += 1;
                // fill the target with a substring, from the 0th character to the current one
                target.text(input.substring(0, current));
                // Wait ...
                setTimeout(function() {
                    // do the function again, with the newly incremented marker
                    type(input, target, current, time, random);
                    // Time it the normal time, plus a random amount of sway
                }, time + Math.random() * random);
            }
        }

        /*
         * The typing test stuff
         */

        var character_length = 31;
        var index = 0;
        var letters = $("#input_text").val();
        var started = false;
        var current_string = letters.substring(index, index + character_length);

        var wordcount = 0;

        $("html, body").click(function() {
            $("#textarea").focus();
        });

        $("#target").text(current_string);
        $(window).keypress(function(evt) {
            if (!started) {
                start();
                started = true;
            }
            evt = evt || window.event;
            var charCode = evt.which || evt.keyCode;
            var charTyped = String.fromCharCode(charCode);
            if (charTyped == letters.charAt(index)) {
                if (charTyped == " ") {
                    wordcount++;
                    $("#wordcount").text(wordcount);
                }
                index++;
                current_string = letters.substring(index, index + character_length);
                $("#target").text(current_string);
                $("#your-attempt").append(charTyped);
                if (index == letters.length) {
                    wordcount++;
                    $("#wordcount").text(wordcount);
                    $("#timer").text(timer);
                    if (timer == 0) {
                        timer = 1;
                    }
                    wpm = Math.round(wordcount / (timer / 60));
                    $("#wpm").text(wpm);
                    stop();
                    finished();
                }
            } else {
                $("#your-attempt").append("<span class='wrong'>" + charTyped + "</span>");
                errors++;
                $("#errors").text(errors);
            }
        });

        var timer = 0;
        var wpm = 0;
        var errors = 0;
        var interval_timer;

        $("#reset").click(function() {
            reset();
        });

        $("#change").click(function() {
            $("#input_text").show().focus();
        });

        $("#pause").click(function() {
            stop();
        });

        $("#input_text").change(function() {
            reset();
        });

        function start() {
            interval_timer = setInterval(function() {
                timer++;
                $("#timer").text(timer);
                wpm = Math.round(wordcount / (timer / 60));
                $("#wpm").text(wpm);
            }, 1000)
        }

        function stop() {
            clearInterval(interval_timer);
            started = false;
        }

        function reset() {
            $("#input_text").blur().hide();;
            $("#your-attempt").text("");
            index = 0;
            errors = 0;
            clearInterval(interval_timer);
            started = false;
            letters = $("#input_text").val();
            $("#wpm").text("0");
            $("#timer").text("0");
            $("#wordcount").text("0");
            timer = 0;
            wpm = 0;
            current_string = letters.substring(index, index + character_length);
            $("#target").text(current_string);
        }

        function finished() {
            alert("Congratulations!\nWords per minute: " + wpm + "\nWordcount: " + wordcount + "\nErrors:" + errors);
        }

        var window_focus;

        $(window).focus(function() {
            window_focus = true;
        }).blur(function() {
            window_focus = false;
        });

        $(document).ready(function() {
            if (window_focus) {
                $("#focus").hide();
            }
            $(window).focus(function() {
                $("#focus").hide();
            });
        });
    });
    </script>
</head>

<body>
    <div class="container">

          <div class="row">
            <div class="col-md-8">
              <div class="card mb-4 box-shadow">

                <div class="card-body">
                    <p id="focus">Click this window to give it focus (Start typing with a capital S!)</p>
                    <h1 id="output"></h1>
                    <div class="target mono" id="target"></div>
                    <div id="your-attempt" class="mono your-attempt" placeholder="Your text will appear here"></div>
                    <p class="twitter">Tweet your highest words per minute
                        @ us! <a href="https://twitter.com/EightArmsHQ" target="_blank">https://twitter.com/EightArmsHQ</a></p>
                    <div class="results">
                        <ul class="stats">
                            <li>Words per minute <span id="wpm">0</span></li>
                            <li>Wordcount <span id="wordcount">0</span></li>
                            <li>Timer <span id="timer">0</span></li>
                            <li>Errors <span id="errors">0</span></li>
                        </ul>
                    </div>
                    <hr style="clear:both;" />
                    <div>
                        <textarea name="" id="input_text" cols="30" rows="10">Suddenly quite near him there was a rifle shot. He heard the crack and smack and whistling ricochet among the rocks behind him. He dropped his torch and began feebly to trot.</textarea>
                    </div>
                    <div class="settings">
                        <a href="#" id="reset">Reset</a> | <a href="#" id="change">Change text</a> | <a href="#" id="pause">Pause II</a>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22348%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20348%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16babfe7544%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A17pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16babfe7544%22%3E%3Crect%20width%3D%22348%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22116.5%22%20y%3D%22120.3%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                <div class="card-body">
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
</body>

</html>
