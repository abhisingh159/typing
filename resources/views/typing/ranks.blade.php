@foreach ($ranks as $rank)
    <div class="row">
        <div class="col-4">{{ $rank->rank  }}</div>
        <div class="col-4">{{ ucfirst($rank->user->name) }}</div>
        <div class="col-4">{{ $rank->points  }}</div>
    </div>
@endforeach