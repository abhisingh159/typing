<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$contest->title}} - Fantasy Typing</title>
    <link href='https://fonts.googleapis.com/css?family=Cutive+Mono|Roboto:400,900,700' rel='stylesheet'
          type='text/css'/>
    <style>
        body {
            padding-top: 20px;
            font-family: Roboto, sans-serif;
            text-align: center;
            background: #fff;
            color: #222;
            padding-bottom: 40px;
            background: url(https://djave.co.uk/hosted/subtlepatterns/lightpaperfibers.png)
        }
        *, ::after, ::before {
            box-sizing: border-box;
        }

        #focus {
            background: rgba(255, 0, 0, .7);
            padding: 20px;
            font-weight: 700;
            font-size: 30px;
            color: #fff
        }

        .mono {
            font-family: "Cutive Mono", monospace
        }

        .wrong {
            background: #99f;
            color: #fff
        }

        hr {
            margin: 1em 0;
            max-width: 800px;
            border: none;
            border-top: 1px solid rgba(255, 255, 255, .3);
            margin: 0 auto
        }

        .results {
            position: fixed;
            top: 0;
            left: 0;
            right: 0
        }

        .stats {
            overflow: hidden;
            height: 100px;
            list-style: none;
            padding: 5px 0;
            font-size: 16px;
            font-weight: 900;
            max-width: 1000px;
            margin: 0 auto 1em;
        }

        .stats li {
            width: 25%;
            float: left
        }

        .target {
            color: #fff;
            text-align: left;
            font-size: 32px;
            min-width: 300px;
            min-height: 40px;
            width: 80%;
            display: inline-block;
            position: relative;
            white-space: pre;
            background: #333;
            box-shadow: inset 0 0 10px 0 rgba(255, 255, 255, .2);
            padding: 22px 10px 10px
        }

        .target:after {
            content: '';
            position: absolute;
            width: 20px;
            height: 2px;
            background: red;
            left: 10px;
            top: 53px;
            -webkit-animation: cursor_flash .5s infinite;
            animation: cursor_flash .5s infinite
        }

        .target:before {
            font-family: Roboto;
            top: 0;
            left: 0;
            right: 0;
            background: #000;
            content: 'Type the text below';
            text-transform: uppercase;
            font-size: 10px;
            padding: 2px;
            text-align: left;
            position: absolute;
            color: #fff
        }

        @-webkit-keyframes cursor_flash {
            0% {
                background: #fff
            }
            50% {
                background: #99f
            }
            100% {
                background: #fff
            }
        }

        @keyframes cursor_flash {
            0% {
                background: #fff
            }
            50% {
                background: #99f
            }
            100% {
                background: #fff
            }
        }

        textarea {
            color: #fff;
            width: 60%;
            margin: 1em auto;
            display: none;
            position: relative;
            white-space: pre;
            background: #333;
            box-shadow: inset 0 0 10px 0 rgba(255, 255, 255, .2);
            border: 1px solid rgba(255, 255, 255, .4)
        }

        .your-attempt {
            background: #222;
            color: #fff;
            padding: 10px;
            min-height: 100px;
            border: 1px solid #555;
            max-width: 80%;
            margin: 2em auto;
            white-space: pre-wrap
        }

        .results {
            font-family: Roboto
        }

        .container{
            width: 100%;
            padding: 0 15px;
        }
        .rankings {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-top: 1rem;
        }
        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .col-8, .col-4 {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }
         .col-8{
             flex: 0 0 66.666667%;
             max-width: 66.666667%;
         }
         .col-4{
             flex: 0 0 33.333333%;
             max-width: 33.333333%;
         }
         .rankings .row > div{
             padding: 5px 15px;
         }


        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: absolute;
            background-color: #fefefe;
            padding: 20px;
            border: 1px solid #888;
            width: 40%;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%,-50%);
            -moz-transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
            -o-transform: translate(-50%,-50%);
            transform: translate(-50%,-50%);
        }
    </style>
</head>
<body>

<div class="results">
    <ul class="stats">
        <li>Words per minute <span id="wpm">0</span></li>
        <li>Wordcount <span id="wordcount">0</span></li>
        <li>Timer <span id="timer">0</span></li>
        <li>Errors <span id="errors">0</span></li>
    </ul>
</div>
<p id="focus">Click this window to give it focus (Start typing with a capital S!)</p>
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 id="output"></h1>
            <h2>Hi {{ ucfirst(Auth::user()->name) }} !  Welcome to the contest</h2>
            <div class="target mono" id="target"></div>
            <div id="your-attempt" class="mono your-attempt" placeholder="Your text will appear here"></div>

            <hr style="clear:both;"/>
            <div>
                <textarea name="" id="input_text" cols="30" rows="10">{{ $contest->typing_text }}</textarea>
            </div>
        </div>
        <div class="col-4">
            <div class="rankings">
                <h2>Contest Rankings</h2>
                <div class="row heading">
                    <div class="col-4"><strong>Rank</strong></div>
                    <div class="col-4"><strong>Name</strong></div>
                    <div class="col-4"><strong>Points</strong></div>
                </div>
                <div class="ranks">
                    @if( $contest->typingTests()->exists() )
                        @foreach ($contest->typingTests as $key => $ranking)
                            <div class="row">
                                <div class="col-4">{{ $ranking->rank  }}</div>
                                <div class="col-4">{{ ucfirst($ranking->user->name) }}</div>
                                <div class="col-4">{{ $ranking->points  }}</div>
                            </div>
                        @endforeach
                    @else
                        @foreach ($contest->users as $key => $user)
                            <div class="row">
                                <div class="col-4">{{ $key + 1  }}</div>
                                <div class="col-4">{{ ucfirst($user->name) }}</div>
                                <div class="col-4">-</div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <p class="msg"></p>
    </div>

</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>

    /*
    * The animation at the start, made from my previous pen
    * https://codepen.io/EightArmsHQ/pen/HJsav
    */

    // Get the modal
    var modal = document.getElementById("myModal");
    var msg = document.getElementsByClassName("modal-content")[0];

    // The base speed per character
    time_setting = 30;
    // How much to 'sway' (random * this-many-milliseconds)
    random_setting = 100;
    // The text to use NB use \n not real life line breaks!
    input_text = "{{ $contest->title }}";
    // Where to fill up
    target_setting = $("#output");
    // Launch that function!
    type(input_text, target_setting, 0, time_setting, random_setting);

    function type(input, target, current, time, random) {
// If the current count is larger than the length of the string, then for goodness sake, stop
        if (current > input.length) {
// Write Complete
            console.log("Complete.");
        } else {
// console.log(current)
// Increment the marker
            current += 1;
// fill the target with a substring, from the 0th character to the current one
            target.text(input.substring(0, current));
// Wait ...
            setTimeout(function () {
// do the function again, with the newly incremented marker
                type(input, target, current, time, random);
// Time it the normal time, plus a random amount of sway
            }, time + Math.random() * random);
        }
    }

    /*
    * The typing test stuff
    */

    var character_length = 41;
    var index = 0;
    var letters = $("#input_text").val();
    var started = false;
    var current_string = letters.substring(index, index + character_length);

    var wordcount = 0;
    var wCount = 0;
    var count = 0;

    $("html, body").click(function () {
        $("#textarea").focus();
    });

    $("#target").text(current_string);
    $(window).keypress(function (evt) {
        if (!started) {
            start();
            started = true;
        }
        evt = evt || window.event;
        var charCode = evt.which || evt.keyCode;
        var charTyped = String.fromCharCode(charCode);
        if (charTyped == letters.charAt(index)) {
            if (charTyped == " ") {
                wordcount++;
                $("#wordcount").text(wordcount);
                var result = {
                    user_id: "{{ Auth::id()  }}",
                    contest_id: "{{ $contest->id  }}",
                    wpm: wpm,
                    word_count: wordcount,
                    errors: errors,
                    point: Math.round((wordcount - errors) * 2),
                };
                var url = "{{ route('typingResult',$contest->id) }}";
                var ajaxRequest = $.ajax({
                    url: url,
                    type: "GET",
                    data: result,
                    dataType: 'html',
                });
                ajaxRequest.success(function (response) {
                    $('.ranks').html(response);
                });
                ajaxRequest.fail(function (jqXHR, textStatus) {
                    console.log("Request Failed: "+textStatus);
                });
            }
            index++;
            current_string = letters.substring(index, index + character_length);
            $("#target").text(current_string);
            $("#your-attempt").append(charTyped);
            if (index == letters.length) {
                wordcount++;
                $("#wordcount").text(wordcount);
                $("#timer").text(timer);
                if (timer == 0) {
                    timer = 1;
                }
                wpm = Math.round(wordcount / (timer / 60));
                $("#wpm").text(wpm);
                stop();
                finished();
            }
        } else {
            $("#your-attempt").append("<span class='wrong'>" + charTyped + "</span>");
            if ( (wCount == 0) || (wCount <= wordcount) ){
                wCount = wordcount + 1;
                count = 0;
            }
            if ( (parseInt($("#wordcount").text()) + 1) == wCount ){
                if(count == 0){
                    errors++;
                    $("#errors").text(errors);
                    count++;
                }
            }
        }
    });

    var timer = 0;
    var wpm = 0;
    var errors = 0;
    var interval_timer;

    var start_time = new Date();
    var end_time = new Date("{{$contest->end_time}}");
    var diff = Math.abs(end_time - start_time);
    var mins = Math.round((diff/1000));
    console.log(mins);
    function start() {
        interval_timer = setInterval(function () {
            timer++;
            $("#timer").text(timer);
            wpm = Math.round(wordcount / (timer / 60));
            $("#wpm").text(wpm);
            if (timer == mins){
                stop();
                finished();
            }
        }, 1000)
    }

    function stop(){
        clearInterval(interval_timer);
        started = false;
    }

    function finished() {
        msg.innerHTML = "<p style='color:#008000;'>Contest Over!! <br><br> Thanks For Participating. <br><br> Result of this contest will publish in 1 to 24 hours.</p>";
        modal.style.display = "block";
        setInterval(function () {
            window.location.href = "{{ route('contestResult', $contest->id)  }}";
        }, 5000);
        {{--window.location.href = "{{ route('contestResult', $contest->id)  }}";--}}
    }

    var window_focus;

    $(window).focus(function () {
        window_focus = true;
    }).blur(function () {
        window_focus = false;
        // Get the modal
        {{--msg.innerHTML = "<p style='color:#ff0000;'>You have violated our Rules and Policies !!</p>";--}}
        {{--modal.style.display = "block";--}}
        {{--setInterval(function () {--}}
            {{--window.location.href = "{{ route('contestResult', $contest->id)  }}";--}}
        {{--}, 1000);--}}
    });

    $(document).ready(function () {
        if (window_focus) {
            $("#focus").hide();
        }
        $(window).focus(function () {
            $("#focus").hide();
        });

        $(document).on("keydown", function (e) {
            if (e.which === 8) {
                e.preventDefault();
            }
        });

    });

</script>

</body>

</html>