 @extends('layouts.main')
 @section('content')
     <div class="pb-3 pt-3 home-match">
 		<div class="row">
             @foreach ($contests as $key => $contest)
                 <div class="col-xm-4 col-md-4 mb-3">
     				<div class="col-md-12 home-match-card4">
     					<div class="d-flex text-muted text-muted home-prize">
     						<div class="col-sm-6 text-left"><p class="text-capitalize">{{$contest->title}}</p></div>
     						<div class="col-sm-6 text-right"><p>Entry</p></div>
     					</div>
     					<div class="d-flex justify-content-center align-items-center">
     						<div class="col-sm-6 text-left "><h5 class="m-0">₹3 Crore</h5></div>
     						<div class="col-sm-6 text-right">
     							<a href="#" class="btn btn-success join">₹ {{$contest->entry_fees}}</a>
     						</div>
     					</div>
     					<div class="progress mt-3" style="height:8px">
     						<div class="progress-bar bg-gold" style="width:25%"></div>
     					</div>
     					<div class="d-flex">
     						<div class="col-sm-6 text-left text-gold"><p>5,28,518 soprt left</p></div>
     						<div class="col-sm-6 text-right	text-muted text-muted home-prize"><p>{{$contest->max_users}} spots</p></div>
     					</div>
     				</div>
     			</div>
             @endforeach
 		</div>
 	</div>
 @endsection
