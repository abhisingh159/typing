<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypingTest extends Model
{
    protected $fillable = [
        'user_id','contest_id','wpm','word_count','errors','point'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function contest()
    {
        return $this->belongsTo('App\Contest', 'contest_id','id');
    }
}
