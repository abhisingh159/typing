<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Emadadly\LaravelUuid\Uuids;

class Contest extends Model
{
    use Sluggable;
    use Uuids;
    protected $dates = [
        'start_time',
        'end_time'
    ];

    protected $fillable = [
        'title', 'description', 'typing_text', 'entry_fees', 'total_prize', 'max_users', 'status', 'start_time', 'end_time'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public $incrementing = false;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->orderBy('name');
    }

    public function hasUser($user)
    {
        return $this->users->contains($user);
    }

    public function prizings()
    {
        return $this->hasMany('App\Prizing');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function typingTests()
    {
        return $this->hasMany('App\TypingTest','contest_id','id')->orderBy('points', 'DESC');
    }

}
