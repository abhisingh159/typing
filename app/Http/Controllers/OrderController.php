<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\stdClass;
use PaytmWallet;
use App\Contest;
use App\Order;
class OrderController extends Controller
{
    /**
     * Redirect the user to the Payment Gateway.
     *
     * @return Response
     */
    public function order(Request $request, Contest $contest)
    {
         if ($contest->reg_users == $contest->max_users){
             return redirect()->route('welcome')->with('error', 'No space in contest!!');
         }
        if ($request->isMethod('post')){
            $order = Order::firstOrNew(['user_id' => auth()->user()->id,'contest_id' => $contest->id, 'status' => 'TXN_SUCCESS']);
            $order->amount = $contest->entry_fees;
            $order->save();
            $order->update(['order_id' => str_random(4).'-'.$order->id]);
            $payment = PaytmWallet::with('receive');
            $user = auth()->user();

            $payment->prepare([
            'order' => $order->order_id,
            'user' => $user->id,
            'mobile_number' => $user->mobile,
            'email' => $user->email,
            'amount' => $order->amount,
            'callback_url' => "http://localhost:8000/order/status"
            ]);
            return $payment->receive();
        }

    }

    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');
        $order = Order::where('order_id', '=' , $transaction->getOrderId())->firstOrFail();

        $response = $transaction->response();
        $order->bank_txn_id = (isset($response["BANKTXNID"]) ? $response["BANKTXNID"] : '');
        $order->transition_id = (isset($response["TXNID"]) ? $response["TXNID"] : '');
        $order->tnx_date = (isset($response["TXNDATE"]) ? $response["TXNDATE"] : NULL);
        $order->status = (isset($response["STATUS"]) ? $response["STATUS"] : '');
        $order->get_way = (isset($response["GATEWAYNAME"]) ? $response["GATEWAYNAME"] : '');
        $order->bank_txn_id = (isset($response["BANKTXNID"]) ? $response["BANKTXNID"] : '');
        $order->bank_name = (isset($response["BANKNAME"]) ? $response["BANKNAME"] : '');
        $order->save();
        if($transaction->isSuccessful()){
            $contest =  Contest::where('id', $order->contest_id)->firstOrFail();
            $contest->reg_users = $contest->reg_users + 1 ;
            $contest->users()->attach(auth()->id());
            $contest->save();
            return redirect()->route('welcome')->with('success', 'Contest joined successfully.');
        }else if($transaction->isFailed()){
            return redirect()->route('welcome')->with('error', 'Transaction failed.');
        }else if($transaction->isOpen()){
            return redirect()->route('welcome')->with('Warning', 'Transaction Pending.');
        }else{
            return redirect()->route('welcome');
        }
        echo $transaction->getResponseMessage();
        exit;
        //Get Response Message If Available
        //get important parameters via public methods
        // $transaction->getOrderId(); // Get order id

    }
}
