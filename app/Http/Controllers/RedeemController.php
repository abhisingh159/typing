<?php

namespace App\Http\Controllers;

use App\Redeem;
use App\User;
use Illuminate\Http\Request;

class RedeemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $redeems = Redeem::orderBy('status','ASC')->paginate(20);

        return view('admin.redeem.index', compact('redeems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $redeem = new Redeem;
        $redeem->user_id = $request->user_id;
        $redeem->amount = $request->amount;
        $redeem->status = $request->status;

        if ($redeem->save()) :
            $user = User::find($request->user_id);
            $user->balance = ($user->balance - $request->amount);
            $user->save();
        endif;
        return redirect()->route('user.earning', \Auth::id())->with('success','Redeem request sent successfully. It will take effect within 3-4 working days. ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Redeem  $redeem
     * @return \Illuminate\Http\Response
     */
    public function show(Redeem $redeem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Redeem  $redeem
     * @return \Illuminate\Http\Response
     */
    public function edit(Redeem $redeem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Redeem  $redeem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Redeem $redeem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Redeem  $redeem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Redeem $redeem)
    {
        //
    }

    public function pay(Redeem $redeem){
        if ($redeem->status == 0){
            $redeem->status = 1;
            $redeem->save();
        }
        return redirect()->back()->with('success', 'Redeem request status changed to paid');
    }
}
