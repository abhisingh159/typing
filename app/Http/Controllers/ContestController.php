<?php

namespace App\Http\Controllers;

use App\Contest;
use App\Prizing;
use App\TypingTest;
use Illuminate\Http\Request;
use App\Http\Requests\addContest;
use Illuminate\View\View;
use Flash;

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contests = Contest::paginate(10);
        return view('admin.contest.index',compact('contests'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function running()
    {
        // echo date('Y-m-d H:i:s');exit;
        // echo \Carbon\Carbon::now();exit;
        $contests = \App\Contest::where('start_time', '<', \Carbon\Carbon::now())->where('end_time', '>', \Carbon\Carbon::now())->get();
        return view('admin.contest.running',compact('contests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.contest.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(addContest $request)
    {
//        dd($request);
        $contest = new Contest;
        $contest->title = $request->title;
        $contest->description = (!empty($request->description)) ? $request->description : "";
        $contest->typing_text = $request->typing_text;
        $contest->entry_fees = $request->entry_fees;
        $contest->total_prize = $request->total_prize;
        $contest->max_users = $request->max_users;
        $contest->start_time = $request->start_time;
        $contest->end_time = $request->end_time;
        $contest->status = $request->status;
        if($contest->save()){
//            return redirect(route('prizing.create',$contest->id));
            for ($i = 0; $i < count($request->from); $i++){
                $prize = new Prizing();
                $prize->contest_id = $contest->id;
                $prize->rank_from = $request->from[$i];
                $prize->rank_to = (isset($request->to[$i]) ? $request->to[$i] : 0);
                $prize->amount = $request->prize[$i];
                $prize->save();
            }
            return redirect(route('contest.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function show(Contest $contest)
    {
        return view('admin.contest.show',compact('contest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function edit(Contest $contest)
    {
        return View('admin.contest.edit',compact('contest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contest $contest)
    {
        $contest->update($request->all());
        return redirect()->route('contest.index')->with('success','Contest updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contest  $contest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contest $contest)
    {
        //
    }

    public function contest(Contest $contest){
       dd($contest);
      return view('contest.contest',compact('contest'));
    }
}
