<?php

namespace App\Http\Controllers;

use App\TypingTest;
use App\Contest;
use Illuminate\Http\Request;

class TypingTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $contest =  Contest::where('id','=',$id)->first();
        return view('typing.typing', compact('contest'));
    }

    /**
     * Save Typing status .
     *
     * @return \Illuminate\Http\Response
     */
    public function typingResult(Request $request){
        $user_id = $request->user_id;
        $contest_id = $request->contest_id;
        $typing = TypingTest::firstOrNew(['user_id' => $user_id,'contest_id' => $contest_id]);
        $typing->user_id = $request->user_id;
        $typing->contest_id = $request->contest_id;
        $typing->wpm = $request->wpm;
        $typing->word_count = $request->word_count;
        $typing->errors = $request->errors;
        $typing->points = $request->point;
        $result =  $typing->save();
        if($result){
            $rankings = $this->getRankings($typing->contest_id);
            $this->updateRankings($typing->contest_id,$rankings);
            $ranks =  TypingTest::where('contest_id', $contest_id)->orderBy('rank','ASC')->get();
            return view('typing.ranks', compact('ranks'));
        } else {
            return response()->json($result);
        }
    }

    public function getRankings($contest_id)
    {
        $rankings = TypingTest::where('contest_id', $contest_id)->orderBy('points','DESC')->get();
        $i = 0;
        $lastscore = NULL;
        foreach ($rankings as $ranking) :
            if($lastscore != $ranking->points) :
                $i++;
            endif;
            $rank[] = $i;
            TypingTest::where(['user_id' => $ranking->user_id,'contest_id' => $ranking->contest_id])
                        ->update(['rank'=>$i]);
            $lastscore = $ranking->points;
        endforeach;
        return array_count_values($rank);
    }

    public function updateRankings($id, $rankings)
    {
        $contest = Contest::find($id);
        foreach ($contest->prizings as $prizing):
            $ranks = $from = $prizing->rank_from;
            $to = $prizing->rank_to;
            if ($to != 0) :
                while($ranks <= $to):
                    $divide = (array_key_exists($ranks,$rankings)) ? $rankings[$ranks] : 1;
                    $prize = ($prizing->amount /  $divide);
                    TypingTest::where(['contest_id' => $contest->id,'rank'=>$ranks])->update(['amount' => $prize]);
                endwhile;
                $ranks++;
            else :
                $divide = (array_key_exists($ranks,$rankings)) ? $rankings[$ranks] : 1;
                $prize =  ($prizing->amount / $divide );
                TypingTest::where(['contest_id' => $contest->id,'rank'=>$ranks])->update(['amount' => $prize]);
            endif;
        endforeach;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypingTest  $typingTest
     * @return \Illuminate\Http\Response
     */
    public function show(TypingTest $typingTest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypingTest  $typingTest
     * @return \Illuminate\Http\Response
     */
    public function edit(TypingTest $typingTest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypingTest  $typingTest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypingTest $typingTest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypingTest  $typingTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypingTest $typingTest)
    {
        //
    }
}
