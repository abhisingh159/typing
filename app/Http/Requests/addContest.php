<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addContest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'typing_text' => 'required',
            'max_users' => 'required|numeric',
            'total_prize' => 'required|numeric',
            'entry_fees' => 'required|numeric',
            'start_time' => 'required|date|after:today',
            'end_time' => 'required|date|after:start_time',
        ];
    }
}
