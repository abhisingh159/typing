<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', 'HomeController@welcome')->name('welcome');

Route::match(array('GET','POST'),'/verify-otp', 'UserController@verifyOtp')->name('verify-otp');
Route::group(['middleware' => 'role:super'], function () {
        Route::get('/admin/home','DashboardController@index')->middleware('role:super')->name('admin.home');
        Route::resource('/admin/dashboard','DashboardController')->middleware('role:super');
        Route::get('/admin/contest/running','ContestController@running')->middleware('role:super')->name('contest.running');
        Route::resource('/admin/contest','ContestController')->middleware('role:super');
        Route::resource('/admin/{contest}/prizing','PrizingController')->middleware('role:super');
        Route::resource('/admin/user','UserController')->middleware('role:super');
        Route::resource('/admin/contact','ContactUsController')->middleware('role:super');
        //REDEEM REQUEST
        Route::get('/admin/redeem','RedeemController@index')->middleware('role:super')->name('admin.redeem');
        Route::get('/admin/redeem/pay/{redeem}','RedeemController@pay')->middleware('role:super')->name('admin.redeem.pay');

        Route::get('/admin/contact-us/{id}','ContactUsController@destroy')->middleware('role:super')->name('contact.delete');
        Route::get('/admin/user/delete/{id}','UserController@destroy')->middleware('role:super')->name('user.delete');

        Route::get('/admin/users/block','UserController@blockUsers')->middleware('role:super')->name('users.block');
        Route::get('/admin/users/unblock/{id}','UserController@unBlockUsers')->middleware('role:super')->name('users.UnBlock');
        Route::get('/admin/users/update-balance/{id}','UserController@updateBalance')->middleware('role:super')->name('users.updateBalance');

        // VERIFY OTP
        
    });

Route::get('user/verify', 'UserController@verify')->name('phoneverification.notice');
Route::post('user/verify', 'UserController@verifyOTP')->name('phoneverification.verify');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'verifiedphone'], function () {

        Route::get('/home', 'HomeController@welcome')->name('home');

        Route::get('/contest/{contest}', 'ContestController@contest')->middleware('auth')->name('contest');
        Route::post('/order/transition/{contest}', 'OrderController@order')->middleware('auth')->name('order');
        Route::post('/order/status', 'OrderController@paymentCallback');
        Route::get('/user/contests', 'UserController@myContests')->name('myContests');
        Route::resource('/user', 'UserController');
        Route::get('/user/contest-result/{contest}', 'UserController@contestResult')->name('contestResult');
        Route::get('/user/{user}/earning', 'UserController@myEarning')->name('user.earning');
        Route::resource('/user/redeem', 'RedeemController');
    });
    Route::group(['middleware' => 'ContestJoined'], function () {
        Route::get('/typing/{contest}', 'TypingTestController@index')->name('typing');
        Route::get('/typing/typing-result/{contest}', 'TypingTestController@typingResult')->name('typingResult');
    });
});


Route::get('/contest-details/{id}', 'HomeController@contestDetails')->name('contestDetails');

Route::get('/about-us', 'HomeController@aboutUs')->name('about-Us');
Route::get('/contact-us', 'HomeController@contactUs')->name('contact-Us');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::post('/contact/mail', 'HomeController@contactmail')->name('contact-mail');

